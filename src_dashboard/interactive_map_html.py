import pickle

import dash_core_components as dcc
import dash_html_components as html
import dash_extendable_graph as deg

from src_dashboard import interactive_map_functions
from src_dashboard import config
from src_dashboard import precomputed_data
from src_dashboard import label_getters


inital_args = {'age_group': 'all', 's_hour': 0, 'e_hour': 23,
               's_month': 1, 'e_month': 12, 's_weekday': 0, 'e_weekday': 6,
                'user_type': 'all', 'dot_style': 'net', 'normalize': True}




def get_widgets_div():
    return html.Div([
        html.Div([
            html.Div(children=[
                html.Label('Age Groups', id='age-groups-range-label'),
                dcc.Dropdown(
                    id='age-group',
                    options=label_getters.get_label_options(config.age_labels),
                    value='all',
                    style={'color': 'black'},
                    clearable=False,
                ),
            ]),

            html.Div(className='menu', children=[
                html.Label('Hour', id='time-range-label'),
                dcc.RangeSlider(
                    id='hour-slider',
                    min=0,
                    max=23,
                    step=1,
                    value=[0, 23],
                    marks=label_getters.get_label_marks(0, 24),
                    pushable=0,
                ),
            ]),

            html.Div(className='menu', children=[
                html.Label('Weekday', id='week-range-label'),
                dcc.RangeSlider(
                    id='week-slider',
                    min=0,
                    max=6,
                    step=1,
                    value=[0, 6],
                    marks=label_getters.get_marks(enumerate(config.days), skip=0),
                    pushable=0
                ),
            ]),

            html.Div(className='menu', children=[
                html.Label('Month', id='month-range-label'),
                dcc.RangeSlider(
                    id='month-slider',
                    min=1,
                    max=12,
                    step=1,
                    value=[1, 12],
                    marks=label_getters.get_marks(config.months, skip=2),
                    pushable=0,
                ),
            ]),

            html.Div(className='menu', children=[
                html.Label('Client Type', id='client-types-range-label', style={'width': '59%', 'display': 'inline-block'}),
                html.Label('Dot Type', id='dot-types-range-label', style={'width': '39%', 'display': 'inline-block'}),

                dcc.RadioItems(
                    id='client-type',
                    options=label_getters.get_label_options(config.user_type_names),
                    value='all',
                    style={'width': '59%', 'display': 'inline-block'},
                ),
                dcc.RadioItems(
                    id='dot-type',
                    options=label_getters.get_label_options(['net', 'total']),
                    value='net',
                    style={'width': '39%', 'display': 'inline-block'},
                )

            ])

        ])
    ], style={'position': 'absolute', 'bottom': '0px', 'width': '100%', 'display': ' block'})


def get_map_div():
    return deg.ExtendableGraph(
        id='map-with-slider',
        config={'displayModeBar': False},
        style={'width': '100%', 'height': '100%', 'float': 'center'}
    )


import plotly.graph_objects as go

def get_map_bundle():
    return html.Div([
                html.Div(
                    className="content",
                    children=[
                        get_widgets_div(),
                    ],
                    style={'width': '20%', 'height': '85%', 'float': 'center', 'padding-left': '60px'}
                ),
                # html.Div(
                #     dcc.Graph(id='click-data'), style={'position': 'absolute', 'top': '10px'}
                # ),
                # html.Div(
                #     dcc.Graph(id='click-data2'), style={'position': 'absolute', 'bottom': '10px'}
                # ),

                html.Div(
                    className='background',
                    children=[
                        get_map_div(),
                    ],
                    style={'width': '100%', 'height': '80%', 'float': 'center'}
                ),

            ])


def plot_func(**kwargs):
    if False:#kwargs == inital_args:
        with open('./dash_cache/memoization_map_fig.pkl', 'rb') as f:
            interactive_map = pickle.load(f)
    else:
        interactive_map = interactive_map_functions.plot_map_stations(  #plot_top_station_paths(
                trips=precomputed_data.trips,
                df_stations=precomputed_data.df_stations,
                **kwargs
            )

    return interactive_map

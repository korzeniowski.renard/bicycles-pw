import dash_core_components as dcc
import dash_html_components as html

from src_dashboard import config
from src_dashboard import label_getters


def get_basic_stats_html():
    return html.Div([
        html.H2('Basic Statistics'),

        html.Label('Month', id='basic-month-range-label'),
        dcc.RangeSlider(
            id='basic-month-slider',
            min=1,
            max=12,
            step=1,
            value=[1, 12],
            marks=label_getters.get_marks(config.months, skip=0),
            pushable=0,
        ),
        html.Label('Client Type', id='basic-client-types-range-label'),
        dcc.RadioItems(
            id='basic-client-type',
            options=[{'label': k, 'value': v} for k, v in {'subscriber': 2, 'customer': 1}.items()],
            value=1,
        ),
        html.Label('Total rides', id='basic-total-rides-label'),
        html.Td(id='total-rides'),

        html.Label('Male users frac', id='basic-male-user-frac-label'),
        html.Td(id='male_user_frac'),

        html.Label('Fake birth years', id='basic-fake-byear-frac-label'),
        html.Td(id='fake_byear_frac'),

        html.Label('Subscibers fraction', id='basic-subscribers-frac-label'),
        html.Td(id='subscribers_frac'),

        html.H4('Age distribution of people that ride bikes at given months'),
        dcc.Graph(
            id='basic-stats-plot',
            config={'displayModeBar': False},
            style={'width': '100%', 'height': '100%', 'float': 'center'}
        )
    ])


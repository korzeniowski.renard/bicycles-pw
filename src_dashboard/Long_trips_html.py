import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import dash_table
import pickle

with open('./igor_pkl/priv_data.pkl', 'rb') as f:
    priv_data = pickle.load(f)

with open('./igor_pkl/ood_max_names.pkl', 'rb') as f:
    ood_max_names = pickle.load(f)

with open('./igor_pkl/ood_gender.pkl', 'rb') as f:
    ood_gender = pickle.load(f)

with open('./igor_pkl/ood_utype.pkl', 'rb') as f:
    ood_utype = pickle.load(f)

with open('./igor_pkl/money_long.pkl', 'rb') as f:
    money_long = pickle.load(f)

priv_data1 = go.Scatter(x=priv_data.titles, y=priv_data.unknown, name='unknown sex')
priv_data2 = go.Scatter(x=priv_data.titles, y=priv_data.wo_by, name='w/o birth year')
priv_data3 = go.Scatter(x=priv_data.titles, y=priv_data.too_old, name='too old')


money_long1 = go.Scatter(x=money_long.month, y=money_long.customers, name='customers')
money_long2 = go.Scatter(x=money_long.month, y=money_long.subscribers, name='subscribers')

ood_max_names = ood_max_names.iloc[0:10, :]

def get_anonymization_html():
    return html.Div([

        html.H4('Anonimization of data'),
        dcc.Graph(
            id='anonymization',
            figure={
                'data': [priv_data1, priv_data2, priv_data3],
                'layout':
                    go.Layout(title='Number of people trying to anonymize data')
            }),

        html.H2('Long rents'),
        
        html.H4('Gender'),
        dash_table.DataTable(
            id='table1',
            columns=[{"name": i, "id": i} for i in ood_gender.columns],
            data=ood_gender.to_dict('records'),
            style_table={
                'maxWidth': '200px'
            },
        ),
        html.H6(''),
        
        html.H4('Usertype'),
        dash_table.DataTable(
            id='table2',
            columns=[{"name": i, "id": i} for i in ood_utype.columns],
            data=ood_utype.to_dict('records'),
            style_table={
                'maxWidth': '200px'
            },
        ),




        html.H4('Money "earned" on long trips'),
        dcc.Graph(
            id='long trips',
            figure={
                'data': [money_long1, money_long2],
                'layout':
                    go.Layout(title='Factor of money earned on long trips')
            }),

        html.H6(''),

        html.H4('Most popular start station for long trips'),
        dash_table.DataTable(
            id='table',
            columns=[{"name": i, "id": i} for i in ood_max_names.columns],
            data=ood_max_names.to_dict('records'),
            style_table={
                'maxWidth': '500px'
            },
        ),

    ])


import pickle

with open('trips.pkl', 'rb') as f:
    trips = pickle.load(f)

with open('df_stations.pkl', 'rb') as f:
    df_stations = pickle.load(f)

with open('bike_life.pkl', 'rb') as f:
    bike_life = pickle.load(f)

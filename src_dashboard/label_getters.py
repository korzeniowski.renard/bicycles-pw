menu_text_style = {'color': 'white',
                   'text-shadow': '1px 1px 1px black'}


def get_label_options(group):
    return [{'label': label, 'value': label} for label in group]


def get_label_marks(s, e):
    mapping = {str(v): {'label': (' ' if v % 3 else (str(v))), 'style': menu_text_style} for v in range(s, e)}
    mapping[str(e-1)] = {'label': str(e-1), 'style': menu_text_style}
    return mapping


def get_marks(vals, skip):
    if skip:
        dd = {str(value): {'label': (str(name) if value % skip else ' '), 'style': menu_text_style} for value, name in vals}
        dd[vals[-1][0]] = {'label': vals[-1][1], 'style': menu_text_style}
    else:
        dd = {str(value): {'label': str(name), 'style': menu_text_style} for value, name in vals}
    return dd

import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from src_dashboard import interactive_map_html
from src_dashboard.server import app

from src_dashboard import basic_statistics_html
from src_dashboard import bicycles_statistics_html
from src_dashboard import Long_trips_html

return_page = 'tab-1'

app.layout = html.Div([
    dcc.Tabs(id="tabs", value=return_page, children=[
        dcc.Tab(label='Basic Analysis', value='tab-1'),
        dcc.Tab(label='Bikes Statistics', value='tab-2'),
        dcc.Tab(label='Long trips', value='tab-3'),
        dcc.Tab(label='Interactive Map', value='tab-4'),
    ]),
    html.Div(id='tabs-content')
])


@app.callback(Output('tabs-content', 'children'),
              [Input('tabs', 'value')])
def render_content(tab):

    if tab == 'tab-1':
        return_page = 'tab-1'
        layout = basic_statistics_html.get_basic_stats_html()
    elif tab == 'tab-2':
        return_page = 'tab-2'
        layout = bicycles_statistics_html.get_bikes_html()
    elif tab == 'tab-3':
        return_page = 'tab-3'
        layout = Long_trips_html.get_anonymization_html()
    elif tab == 'tab-4':
        return_page = 'tab-4'
        layout = interactive_map_html.get_map_bundle()
    else:
        raise ValueError(f'There is no tab called {tab}')

    return layout


from src_dashboard import map_callbacks
from src_dashboard import basic_statistics_callbacks
from src_dashboard import bicycles_statistics_callbacks

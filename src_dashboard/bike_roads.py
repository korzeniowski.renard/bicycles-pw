def get_routes(df, min_rides, top_5):
    routes_pop_df = df.route.value_counts()
    if top_5:
        print('min_rides argument ignored because `normalize=True`. Returning top 5 paths')
        routes_pop_df = routes_pop_df.sort_values()[-5:]
    else:
        routes_pop_df = routes_pop_df[routes_pop_df > min_rides]
    return routes_pop_df


def plot_top_station_paths(trips, df_stations, hour, month, weekday, user_type, age_group, min_rides=0, normalize=True,
                           zoom_start=13):
    df_trips_month = filter_df_by_stats(trips, hour, month, weekday, user_type, age_group)

    bbox = get_bbox(df_stations)
    df_trips_month = add_routes(df_trips_month)
    most_pop_routes = get_routes(df_trips_month, min_rides, top_5)

    # roads graph
    G = ox.graph_from_bbox(bbox[3], bbox[2], bbox[1], bbox[0], network_type='bike')

    # city map
    folium_map = folium.Map(location=[40.738, -73.98],
                            zoom_start=zoom_start,
                            tiles="CartoDB dark_matter");

    # iteration over top station paths
    i = 0
    norm_rides = most_pop_routes.max()
    for route, count in most_pop_routes.iteritems():
        df_route = df_trips_month[df_trips_month['route'] == route]
        slong, slat = get_station_coords(df_route, df_stations, 'start_station_id')
        elong, elat = get_station_coords(df_route, df_stations, 'end_station_id')

        # define origin/desination points then get the nodes nearest to each
        origin_point = (slat, slong)
        destination_point = (elat, elong)

        origin_node = ox.get_nearest_node(G, origin_point)
        destination_node = ox.get_nearest_node(G, destination_point)

        route = nx.shortest_path(G, origin_node, destination_node, weight='length')

        gdf_nodes, gdf_edges = ox.graph_to_gdfs(G)
        path_points = gdf_nodes.loc[route]

        # path on road for given path
        points = path_points[['y', 'x']].values

        # add path on the map
        line_weight = 3 * count / norm_rides
        folium.PolyLine(points, color=path_colors[i % len(path_colors)], weight=line_weight, opacity=0.85).add_to(
            folium_map)
        i += 1

    return folium_map

#top_route_map = plot_top_station_paths(trips, df_stations, hour=9, month=-1, weekday=-1, user_type='all', age_group='student', top_5=True)
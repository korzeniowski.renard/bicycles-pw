import os

import base64
import dash
import dash_core_components as dcc
import dash_html_components as html
import flask

import pandas as pd
import pickle
import folium
from pathlib import Path
from ipywidgets import interact, interact_manual




##TODO to use my function in dashboard it has to be rewritten into format compliant with
## dashboard widgets. So func update_table that takes values from defined widets and
## feeds correct subdfs to plotly map plotting function


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__ , external_stylesheets=external_stylesheets)

markdown_text = '''
### Dash and Markdown
This is the king of the east
'''

image_filename = '../frame_00001.png'
encoded_image = base64.b64encode(open(image_filename, 'rb').read())
app.layout = html.Div([
    html.H1(children='Hello NewYork!'),
    dcc.Markdown(children=markdown_text),
    html.Img(src='data:image/png;base64,{}'.format(encoded_image.decode()), height=300),

    html.Iframe(id='map', srcDoc=open('../folium_map.html', 'r').read()),

    html.H1(children='City hearth beat'),
    html.Video(src='/assets/output.mp4', controls=True),

    dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
                {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
            ],
            'layout': {
                'title': 'Dash Data Visualization'
            }
        }
    )

])

#app.layout =

server = app.server
#
# @server.route('/static/<path:path>')
# def serve_static(path):
#     root_dir = os.getcwd()
#     return flask.send_from_directory(os.path.join(root_dir, 'static'), path)


# app.layout = html.Div(children=[
#     html.H1(children='Hello Dash'),
#
#

# ])

if __name__ == '__main__':
    app.run_server(debug=True)
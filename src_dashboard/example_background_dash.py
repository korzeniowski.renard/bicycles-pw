import dash
import dash_html_components as html

external_stylesheets = []

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


app.layout = html.Div([

    html.Div(
        className="content",
        children=[
            html.P('This is front text')
        ]
    ),
    html.Div(
        className='background',
        children=[
            html.P('Back text')
        ],
        style={
            'background-image': 'url("/assets/frame_00001.png")',
            'background-repeat': 'no-repeat',
            'background-position': 'right top',
            'background-size': '150px 100px',
        }
    ),

    html.Div(
        style={
            'background-image': 'url("/assets/frame_00001.png")',
            'background-repeat': 'no-repeat',
            'background-position': 'right top',
            'background-size': '150px 100px',
            'display': 'inline-block',
        },
        className="app-header--title",
        children=[
            html.H1('Hello World'),
            html.P('This image has an image in the background')
        ]
    ),
])


if __name__ == '__main__':
    app.run_server(debug=True)

import pandas as pd
import plotly.graph_objects as go
from functools import partial

from src_dashboard import config

popup_text = """station name: {station_name}<br>
total departures: {departure_count}<br>
total arrivals: {arrival_count}<br>
net trips: {total}"""


def get_bbox(df_stations):
    return ((df_stations.longitude.min(), df_stations.longitude.max(),
             df_stations.latitude.min(), df_stations.latitude.max()))


def get_station_coords(df_route, df_stations, col):
    station_id = df_route[col].values[0]
    station = df_stations[df_stations.stationid == station_id]
    station_coords = [station.longitude.values[0], station.latitude.values[0]]
    return station_coords


def get_age_col(df_user_type):
    age_col = df_user_type.start_time.dt.year - df_user_type.birth_year
    return age_col.astype(np.int8)


def get_most_pop_ages(age_series):
    byears = age_series.value_counts().sort_values()
    return byears


def get_fake_ages_frac(top_5_most_pop_byears):
    fake_byear_frac = sum(top_5_most_pop_byears.index > 90) / len(top_5_most_pop_byears)
    return fake_byear_frac


def get_male_users_frac(df_user_type):
    male_user_frac = df_user_type[df_user_type.gender == 1].shape[0] / df_user_type.shape[0]
    return male_user_frac


def get_user_type_subset(df_trips_month, user_type):
    df_user_type = df_trips_month[df_trips_month.user_type == user_type]
    return df_user_type


def get_most_pop_routes(df_user_type):
    top_5_most_pop_routes = df_user_type.route.value_counts().sort_values()[-10:]
    return top_5_most_pop_routes


def add_routes(df_user_type):
    df_user_type = df_user_type.copy()
    df_user_type['route'] = df_user_type.start_station_id.astype(str) + '_' + df_user_type.end_station_id.astype(str)
    return df_user_type


#####

def filter_df_by_stats(df, s_hour, e_hour, s_month, e_month, s_weekday, e_weekday, user_type, age_group):
    if s_month >= 1 and e_month >= 1:
        df = df.loc[pd.IndexSlice[s_month:e_month, :], :]

    if s_hour >= 0 and e_hour >= 0:
        df = df.loc[pd.IndexSlice[:, s_hour:e_hour], :]

    if s_weekday >= 0 and e_weekday >= 0:
        weekdays = range(s_weekday, e_weekday + 1)
        df = df[df['weekday'].isin(weekdays)]

    if user_type != config.no_filter_field:
        df = df[df['user_type'] == config.user_type_names[user_type]]

    if age_group != config.no_filter_field:
        df = df[df['age_bin'] == age_group]

    return df


def get_radious(row, norm):
    if row > 0:
        radius = norm * row
    elif row == 0:
        radius = 0.1
    else:
        radius = norm * row * -1
    return radius


def get_color(row):
    if row > 0:
        color = 'orangered'
    elif row == 0:
        color = "lightgrey"
    else:
        color = 'royalblue'
    return color


def get_popup_text(row):
    text = popup_text.format(
        station_name=row["station_name"],
        departure_count=row["departure_count"],
        arrival_count=row["arrival_count"],
        total=row["total"]
    )

    return text


def plot_map_stations(fig, trips, df_stations, s_hour, s_month, s_weekday, e_hour, e_month, e_weekday, user_type, age_group,
                      dot_style, normalize=True):
    stations_map = df_stations.copy().set_index('stationid')

    df = trips.copy()

    df = filter_df_by_stats(df, s_hour=s_hour, s_month=s_month, s_weekday=s_weekday, e_hour=e_hour, e_month=e_month,
                            e_weekday=e_weekday, user_type=user_type, age_group=age_group)

    departures = df[['end_station_id']].groupby('end_station_id').size()
    arrivals = df[['start_station_id']].groupby('start_station_id').size()

    station_ids = arrivals.index.values

    df = pd.concat([departures, arrivals], axis=1).fillna(0)
    df = df.rename(columns={0: 'arrival_count', 1: 'departure_count'})

    if dot_style == 'total':
        df['total'] = df.arrival_count + df.departure_count
    elif dot_style == 'net':
        df['total'] = df.arrival_count - df.departure_count
    else:
        raise ValueError('dot_style not supported')

    df['start_station_latitude'] = df.index.map(stations_map['latitude'])
    df['start_station_longitude'] = df.index.map(stations_map['longitude'])
    df['station_name'] = df.index.map(stations_map['name'])
    df = df[~df.isnull().any(axis=1)]

    norm = 1 / 10
    if normalize:
        norm = 2500 / df['total'].abs().sum()

    pget_radious = partial(get_radious, norm=norm)

    colors = df['total'].apply(get_color)
    radious = df['total'].apply(pget_radious)
    lat = df["start_station_latitude"]
    lon = df["start_station_longitude"]

    popup_text = df.apply(get_popup_text, axis=1)

    fig.add_trace(go.Scattermapbox(
        lat=lat,
        lon=lon,
        ids=list(map(str, station_ids)),
        mode='markers',
        text=popup_text,
        marker=go.scattermapbox.Marker(
            size=radious,
            color=colors,
            opacity=0.65
        ),
    ))

    return fig


no_filter_field = 'all'
user_type_names = {'subscriber': 2, 'customer': 1, no_filter_field: no_filter_field}
age_labels = [no_filter_field, 'student', 'young_worker', 'old_worker', 'old']
days = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun')
months = [(1, 'Jan'), (2, 'Feb'), (3, 'Mar'), (4, 'Apr'), (5, 'May'), (6, 'Jun'),
          (7, 'Jul'), (8, 'Aug'), (9, 'Sep'), (10, 'Oct'), (11, 'Nov'), (12, 'Dec')]


years = ['2018', '2017']

shaz13_custom_style = "mapbox://styles/mapboxaccesstoken/ck5a92c1k23f41cmj46e2y5dw"

accesstoken='pk.eyJ1IjoibWFwYm94YWNjZXNzdG9rZW4iLCJhIjoiY2s1OHBzeTluMGRxcTNrcWhjMHF2YnN4YSJ9.pKif-5GJUbxZsAUDQGiJlQ'

height = 860
width = 1780

from dash.dependencies import Input, Output, State

from src_dashboard import interactive_map_html
from src_dashboard import config
from src_dashboard.server import app
from src_dashboard import var

import json
import networkx as nx
import osmnx as ox
import pickle
import datetime
import plotly.graph_objects as go
from src_dashboard import precomputed_data

import random

# @app.callback(Output('extendablegraph_example1', 'figure'),
#               [Input('reset-graph', 'n_clicks')])
# def reset_graph(click):
#     if click is None:
#         raise PreventUpdate
#     return dict(data=[{'x': [0, 1, 2, 3, 4], 'y': [0, .5, 1, .5, 0]}])

import pandas as pd
def filter_df_by_stats(df, s_hour, e_hour, s_month, e_month, s_weekday, e_weekday, user_type, age_group):
    if s_month >= 1 and e_month >= 1:
        df = df.loc[pd.IndexSlice[s_month:e_month, :], :]

    if s_hour >= 0 and e_hour >= 0:
        df = df.loc[pd.IndexSlice[:, s_hour:e_hour], :]

    if s_weekday >= 0 and e_weekday >= 0:
        weekdays = range(s_weekday, e_weekday + 1)
        df = df[df['weekday'].isin(weekdays)]

    if user_type != config.no_filter_field:
        df = df[df['user_type'] == config.user_type_names[user_type]]

    if age_group != config.no_filter_field:
        df = df[df['age_bin'] == age_group]

    return df


def get_most_trip_station_ids(df, count):
    return df['end_station_id'].value_counts().sort_values()[-count:].index.values



def get_station_coords(df_route, df_stations, col):
    station_id = df_route[col].values[0]
    station = df_stations[df_stations.stationid == station_id]
    station_coords = [station.longitude.values[0], station.latitude.values[0]]
    return station_coords


@app.callback(
    Output('map-with-slider', 'extendData'),
    [Input('map-with-slider', 'clickData')],
    [State('map-with-slider', 'figure'),
     State('hour-slider', 'value'),
     State('week-slider', 'value'),
     State('month-slider', 'value'),
     State('age-group', 'value'),
     State('client-type', 'value'),
     State('dot-type', 'value')
     ]
)
def display_click_data(clickData, old_fig, selected_hour, selected_weekday, selected_month, age_group, client_type, dot_type):


    try:
        station_id = int(clickData['points'][0]['id'])
    except:
        station_id = -1

    stations_map = precomputed_data.df_stations.copy().set_index('stationid')
    df = precomputed_data.trips.copy()

    df = filter_df_by_stats(df, s_hour=selected_hour[0], s_month=selected_month[0], s_weekday=selected_weekday[0],
                            e_hour=selected_hour[1], e_month=selected_month[1],
                            e_weekday=selected_weekday[1], user_type=client_type, age_group=age_group)


    df = df[df['start_station_id'] == station_id]


    top_station_ids = get_most_trip_station_ids(df, 5)


    with open('./dash_cache/NY_graph.pkl', 'rb') as f:
        G = pickle.load(f)

    lll = []
    for top_station_id in top_station_ids:

        try:
            slong, slat = stations_map.latitude[station_id], stations_map.longitude[station_id]
            elong, elat = stations_map.latitude[top_station_id], stations_map.longitude[top_station_id]
        except:
            lll += [{'lat': [], 'lon': []}]
            continue

        origin_point = (slong, slat)
        destination_point = (elong, elat)
        origin_node = ox.get_nearest_node(G, origin_point)
        destination_node = ox.get_nearest_node(G, destination_point)

        route = nx.shortest_path(G, origin_node, destination_node, weight='length')

        gdf_nodes, gdf_edges = ox.graph_to_gdfs(G)
        path_points = gdf_nodes.loc[route]

        lon = path_points['x'].values
        lat = path_points['y'].values

        lll += [{'lat': lat, 'lon': lon}]

    return lll


@app.callback(
    Output('map-with-slider', 'figure'),
    [Input('hour-slider', 'value'),
     Input('week-slider', 'value'),
     Input('month-slider', 'value'),
     Input('age-group', 'value'),
     Input('client-type', 'value'),
     Input('dot-type', 'value')],
)
def update_figure(selected_hour, selected_weekday, selected_month, age_group, client_type, dot_type):

    try:

        # with open('./dash_cache/memoization_map_fig.pkl', 'rb') as f:
        #     return pickle.load(f)

        map_fig = go.Figure()
        map_fig.add_trace(go.Scattermapbox(
            mode="lines",
            lon=[],
            lat=[],
            opacity=0.7,
        ))

        map_fig.add_trace(go.Scattermapbox(
            mode="lines",
            lon=[],
            lat=[],
            opacity=0.7,
        ))

        map_fig.add_trace(go.Scattermapbox(
            mode="lines",
            lon=[],
            lat=[],
            opacity=0.7,
        ))

        map_fig.add_trace(go.Scattermapbox(
            mode="lines",
            lon=[],
            lat=[],
            opacity=0.7,
        ))

        map_fig.add_trace(go.Scattermapbox(
            mode="lines",
            lon=[],
            lat=[],
            opacity=0.7,
        ))

        map_fig = interactive_map_html.plot_func(
            fig=map_fig,
            age_group=age_group,
            s_hour=selected_hour[0],
            e_hour=selected_hour[1],
            s_month=selected_month[0],
            e_month=selected_month[1],
            s_weekday=selected_weekday[0],
            e_weekday=selected_weekday[1],
            user_type=client_type,
            dot_style=dot_type,
            normalize=True,
        )

        map_fig.update_layout(
            mapbox=dict(accesstoken=config.accesstoken,
                        zoom=13,
                        center=dict(lat=40.721319,
                                    lon=-73.987130),
                        style=config.shaz13_custom_style,
                        ),
            height=config.height,
            width=config.width,
            showlegend=False,
            margin=dict(
                l=0,
                t=0,
                b=0,
            )
        )


        # with open('./dash_cache/memoization_map_fig.pkl', 'wb') as f:
        #     pickle.dump(map_fig, f)

    except Exception as exept:
        print(exept)
        import pdb;pdb.set_trace()

    return map_fig

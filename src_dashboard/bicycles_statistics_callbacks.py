import os
import pickle

from dash.dependencies import Input, Output
import plotly.graph_objects as go

from src_dashboard import precomputed_data
from src_dashboard.server import app
from plotly.subplots import make_subplots


def get_week_dates_type(df, weekend):
    df['weekday'] = df.start_time.dt.weekday
    df['weekend'] = df.weekday.apply(lambda x: True if x > 4 else False)
    return df[df['weekend'] == weekend]


@app.callback(Output('bike-avg-plot', 'figure'),
              [Input('bike-weekend', 'value')])
def plot_avg_bike_ts(weekend):
    print(weekend)

    if weekend is 1:
        if os.path.exists('./dash_cache/memoization_avg_bike_ts_true.pkl'):
            with open('./dash_cache/memoization_avg_bike_ts_true.pkl', 'rb') as f:
                return pickle.load(f)

    elif weekend is 0:
        if os.path.exists('./dash_cache/memoization_avg_bike_ts_false.pkl'):
            with open('./dash_cache/memoization_avg_bike_ts_false.pkl', 'rb') as f:
                return pickle.load(f)

    df = precomputed_data.bike_life
    df = get_week_dates_type(df, weekend=weekend)

    df = df[['start_time', 'tripduration']]
    df.start_time = df.start_time.dt.normalize()
    df['trips'] = 1

    df = df.groupby('start_time').sum()

    df.tripduration = df.tripduration / 3600
    df['avg_trip_time'] = df.tripduration / df.trips

    df = df.rename(columns={'start_time': 'time'})

    fig = make_subplots(specs=[[{"secondary_y": True}]])

    fig.add_trace(go.Scatter(
        x=df.index,
        y=df.avg_trip_time.rolling(5).mean(),
        name='total number of bike trips in NY',
        line_color='deepskyblue',
        opacity=0.8), secondary_y=False)

    fig.add_trace(go.Scatter(
        x=df.index,
        y=df.trips.rolling(5).mean(),
        name="'average number of hours per bike trip'",
        line_color='red',
        opacity=0.8), secondary_y=True)

    if weekend is 1:
        if not os.path.exists('./dash_cache/memoization_avg_bike_ts_true.pkl'):
            with open('./dash_cache/memoization_avg_bike_ts_true.pkl', 'wb') as f:
                pickle.dump(fig, f)

    elif weekend is 0:
        if not os.path.exists('./dash_cache/memoization_avg_bike_ts_false.pkl'):
            with open('./dash_cache/memoization_avg_bike_ts_false.pkl', 'wb') as f:
                pickle.dump(fig, f)

    return fig

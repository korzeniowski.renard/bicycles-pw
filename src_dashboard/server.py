from flask import Flask
from dash import Dash

server = Flask('bicycles-pw')
app = Dash(server=server)
app.config.suppress_callback_exceptions = True

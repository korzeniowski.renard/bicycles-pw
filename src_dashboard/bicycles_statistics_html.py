import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go

from src_dashboard import precomputed_data


def plot_most_pop_bike_ts():
    most_used_bikes = precomputed_data.bike_life.groupby('bikeid').size().sort_values()
    bike_id = most_used_bikes.index[-1]
    df = precomputed_data.bike_life
    df = df[df['bikeid'] == bike_id]
    df = df[['start_time', 'tripduration']]
    df.start_time = df.start_time.dt.normalize()
    df['trips'] = 1
    df = df.groupby('start_time').sum()
    df.tripduration = df.tripduration / 3600
    df['trips_per_hour'] = df.trips / df.tripduration
    df = df.drop(columns='trips')
    df = df.rename(columns={'tripduration': 'hours_per_day', 'start_time': 'time'})

    df = df.rolling(5).mean()

    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=df.index,
        y=df['trips_per_hour'],
        name="trips_per_hour",
        line_color='deepskyblue',
        opacity=0.8))

    fig.add_trace(go.Scatter(
        x=df.index,
        y=df['hours_per_day'],
        name="hours_per_day",
        line_color='red',
        opacity=0.8))

    return fig


def get_bikes_html():
    return html.Div([
        html.H2('Bikes Statistics'),

        html.H4('Most used bike'),
        dcc.Graph(
            figure=plot_most_pop_bike_ts(),
            id='bike-most-plot',
            config={'displayModeBar': False},
            style={'width': '100%', 'height': '100%', 'float': 'center'}
        ),

        html.H4('Avg bike usage'),
        html.Label('Weekend', id='bike-weekend-label'),
        dcc.RadioItems(
            id='bike-weekend',
            options=[{'label': name, 'value': val} for name, val in [('True', 1), ('False', 0)]],
            value=0,
        ),
        dcc.Graph(
            id='bike-avg-plot',
            config={'displayModeBar': False},
            style={'width': '100%', 'height': '100%', 'float': 'center'}
        ),

        html.H4('Relocated bikes'),
        html.Iframe(id='map', srcDoc=open('./relocated_bikes_folium_fig.html', 'r').read(), style={'height': '600px', 'width': '800px'}),

    ])

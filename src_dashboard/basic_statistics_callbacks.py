import plotly.express as px
import numpy as np
from dash.dependencies import Input, Output

from src_dashboard import precomputed_data
from src_dashboard.server import app

import os
import pickle

def get_bbox(df_stations):
    return ((df_stations.longitude.min(), df_stations.longitude.max(),
             df_stations.latitude.min(), df_stations.latitude.max()))


def get_station_coords(df_route, df_stations, col):
    station_id = df_route[col].values[0]
    station = df_stations[df_stations.stationid == station_id]
    station_coords = [station.longitude.values[0], station.latitude.values[0]]
    return station_coords


def get_age_col(df_user_type):
    age_col = df_user_type.start_time.dt.year - df_user_type.birth_year
    return age_col.astype(np.int8)


def get_most_pop_ages(age_series):
    byears = age_series.value_counts().sort_values()
    return byears


def get_fake_ages_frac(top_5_most_pop_byears):
    fake_byear_frac = sum(top_5_most_pop_byears.index > 90) / len(top_5_most_pop_byears)
    return fake_byear_frac


def get_male_users_frac(df_user_type):
    male_user_frac = df_user_type[df_user_type.gender == 1].shape[0] / df_user_type.shape[0]
    return male_user_frac


def get_user_type_subset(df_trips_month, user_type):
    df_user_type = df_trips_month[df_trips_month.user_type == user_type]
    return df_user_type


def get_most_pop_routes(df_user_type):
    top_5_most_pop_routes = df_user_type.route.value_counts().sort_values()[-10:]
    return top_5_most_pop_routes


def add_routes(df_user_type):
    df_user_type = df_user_type.copy()
    df_user_type['route'] = df_user_type.start_station_id.astype(str) + '_' + df_user_type.end_station_id.astype(str)
    return df_user_type


def decimal_trim(*args):
    return ["{0:.2f}".format(x) for x in args]


@app.callback([Output('basic-stats-plot', 'figure'),
               Output('male_user_frac', 'children'),
               Output('fake_byear_frac', 'children'),
               Output('subscribers_frac', 'children'),
               Output('total-rides', 'children'),
               ],
              [Input('basic-month-slider', 'value'),
               Input('basic-client-type', 'value'),
               ])
def period_stats(month, user_type):
    if month == [1, 12] and user_type == 2:
        if os.path.exists('./dash_cache/memoization_default_basic-stats-plot.pkl'):
            with open('./dash_cache/memoization_default_basic-stats-plot.pkl', 'rb') as f:
                return pickle.load(f)

    if month == [6, 8] and user_type == 2:
        if os.path.exists('./dash_cache/memoization_default_basic-stats-plot68.pkl'):
            with open('./dash_cache/memoization_default_basic-stats-plot68.pkl', 'rb') as f:
                return pickle.load(f)

    if month == [1, 2] and user_type == 2:
        if os.path.exists('./dash_cache/memoization_default_basic-stats-plot12.pkl'):
            with open('./dash_cache/memoization_default_basic-stats-plot12.pkl', 'rb') as f:
                return pickle.load(f)

    df_trips_month = precomputed_data.trips.loc[month]
    subscribers_frac = df_trips_month[df_trips_month.user_type == 2].shape[0] / df_trips_month.shape[0]

    df_user_type = get_user_type_subset(df_trips_month, user_type)
    total_rides = len(df_trips_month)
    male_user_frac = get_male_users_frac(df_user_type)

    df_user_type = add_routes(df_user_type)
    df_user_type['age'] = get_age_col(df_user_type)
    df_user_type = df_user_type[df_user_type['age'] > 0]
    top_5_most_pop_ages = get_most_pop_ages(df_user_type['age'])
    fake_byear_frac = get_fake_ages_frac(top_5_most_pop_ages)

    fig = px.histogram(df_user_type, x="age", nbins=90)

    male_user_frac, fake_byear_frac, subscribers_frac, total_rides = \
        decimal_trim(male_user_frac, fake_byear_frac, subscribers_frac, total_rides)

    # if month == [1, 12] and user_type == 2:
    #     if not os.path.exists('./dash_cache/memoization_default_basic-stats-plot.pkl'):
    #         with open('./dash_cache/memoization_default_basic-stats-plot.pkl', 'wb') as f:
    #             pickle.dump((fig, male_user_frac, fake_byear_frac, subscribers_frac, total_rides), f)
    #
    # if month == [6, 8] and user_type == 2:
    #     #if not os.path.exists('./dash_cache/memoization_default_basic-stats-plot68.pkl'):
    #     with open('./dash_cache/memoization_default_basic-stats-plot68.pkl', 'wb') as f:
    #         pickle.dump((fig, male_user_frac, fake_byear_frac, subscribers_frac, total_rides), f)
    #
    # if month == [1, 2] and user_type == 2:
    #     if not os.path.exists('./dash_cache/memoization_default_basic-stats-plot12.pkl'):
    #         with open('./dash_cache/memoization_default_basic-stats-plot12.pkl', 'wb') as f:
    #             pickle.dump((fig, male_user_frac, fake_byear_frac, subscribers_frac, total_rides), f)

    return fig, male_user_frac, fake_byear_frac, subscribers_frac, total_rides

# Analysis of the New York City bike share system

### Overview

This project contains a visualization of the [Citibike dataset](https://www.citibikenyc.com/system-data) 
in the form of the interactive map of New York City. The popularity of each station is represented
by blue and orange dots. Blue color means that in total there were more arrivals than departures.
The reverse is true for orange. Since analyzed data is a time series user can slice it by day of the week, month,
year but also by other features extracted from the data like an age group and more!


### Interactive map of New York
![notebook_visualizaiton](/assets/notebook_visualizaiton.png)